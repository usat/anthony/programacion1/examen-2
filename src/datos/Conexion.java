package datos;

import java.sql.*;
import java.util.concurrent.ExecutionException;

public class Conexion {
    private String controlador = "org.postgresql.Driver";
    private String cadenaConexion = "jdbc:postgresql://172.20.128.2:5432/examen2";
    private String usuario = "postgres";
    private String clave = "silviopd";
    
    private Connection conexion;
    
    protected Connection abrirConexion() throws Exception{
        Class.forName(this.controlador);
        this.conexion = DriverManager.getConnection(this.cadenaConexion, usuario, clave);
        return this.conexion;
    }
    
    protected void cerrarConexion(Connection con) throws Exception{
        con.close();
        con = null;
    }
    
    protected ResultSet ejecutarSQL(String sql) throws Exception{
        Statement sentencia = null; //Crear una sentencia SQL
        ResultSet resultado = null; //Sirve para almacenar un resultado que viene de una consulta SQL SELECT
        
        sentencia = this.abrirConexion().createStatement();
        resultado = sentencia.executeQuery(sql);
        this.cerrarConexion(conexion);
        return resultado;
    }
    
    protected ResultSet ejecutarSQL(PreparedStatement sp) throws Exception{
        ResultSet resultado = null; 
        resultado = sp.executeQuery();
        this.cerrarConexion(conexion);
        return resultado;
    }
    
    protected ResultSet ejecutarSQL2(PreparedStatement sp) throws Exception{
        ResultSet resultado = null; 
        resultado = sp.executeQuery();
        return resultado;
    }
    
    protected int ejecutarSQL(PreparedStatement sp, Connection con) throws Exception
    {
        int resultado = 0;
        try 
        {
            resultado = sp.executeUpdate();
        } catch (Exception e) 
        {
            con.rollback();
            throw e;
        }
        return resultado;
    }    
}

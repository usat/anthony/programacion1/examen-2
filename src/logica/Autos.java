/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author silviopd
 */
public class Autos extends Conexion{
    
    private String placa,marca,modelo,year,estado_funcionamiento;
    
    public static ArrayList<Autos> listarAutos= new ArrayList<Autos>();

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getEstado_funcionamiento() {
        return estado_funcionamiento;
    }

    public void setEstado_funcionamiento(String estado_funcionamiento) {
        this.estado_funcionamiento = estado_funcionamiento;
    }
    
    public ResultSet listar() throws Exception {
        String sql = "select * from auto";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "INSERT INTO public.auto(placa, marca, modelo, year, estado_funcionamiento) VALUES (?, ?, ?, ?, ?);";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getPlaca());
            spInsertar.setString(2, this.getMarca());
            spInsertar.setString(3, this.getModelo());
            spInsertar.setString(4, this.getYear());
            spInsertar.setString(5, this.getEstado_funcionamiento());
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ResultSet leerDatosCodigo(String dni) throws Exception {
        String sql = "select * from auto where placa = ?";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setString(1, dni);
        return ejecutarSQL(sentencia);
    }

    public boolean editar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "UPDATE public.auto SET marca=?, modelo=?, year=?, estado_funcionamiento=? WHERE placa=?;";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getMarca());
            spInsertar.setString(2, this.getModelo());
            spInsertar.setString(3, this.getYear());
            spInsertar.setString(4, this.getEstado_funcionamiento());
            spInsertar.setString(5, this.getPlaca());
            
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from auto where placa = ?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, getPlaca());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }
    
    /************************************/
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from auto";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarAutos.clear();
        while(resultado.next())
        {
            Autos objMarca = new Autos();
            objMarca.setPlaca(resultado.getString("placa"));
            objMarca.setMarca(resultado.getString("marca"));
            objMarca.setModelo(resultado.getString("modelo"));
            listarAutos.add(objMarca);
        }
    }
    
    public void llenarComboAutos(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarAutos.size(); i++)
        {
            Autos m = listarAutos.get(i);
            combo.addItem(m.getMarca()+" - "+m.getModelo());
        }
    }
}

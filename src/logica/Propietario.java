/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author silviopd
 */
public class Propietario extends Conexion{
    
    private String dni,nombre,direccion;
    
    public static ArrayList<Propietario> listarPropietarios= new ArrayList<Propietario>();

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    
    public ResultSet listar() throws Exception {
        String sql = "select * from propietario";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "INSERT INTO public.propietario(dni, nombre, direccion) VALUES (?, ?, ?);";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getDni());
            spInsertar.setString(2, this.getNombre());
            spInsertar.setString(3, this.getDireccion());
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public ResultSet leerDatosCodigo(String dni) throws Exception {
        String sql = "select * from propietario where dni = ?";
        PreparedStatement sentencia = abrirConexion().prepareStatement(sql);
        sentencia.setString(1, dni);
        return ejecutarSQL(sentencia);
    }

    public boolean editar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "UPDATE public.propietario SET nombre=?, direccion=? WHERE dni=?;";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getNombre());
            spInsertar.setString(2, this.getDireccion());
            spInsertar.setString(3, this.getDni());
            
            this.ejecutarSQL(spInsertar,transaccion);
            
            transaccion.commit();
            transaccion.close();

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from propietario where dni=?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setString(1, getDni());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }
    
    
    /************************************/
    
    private void cargarDatos() throws Exception
    {
        String sql = "select * from propietario";
        PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        listarPropietarios.clear();
        while(resultado.next())
        {
            Propietario objMarca = new Propietario();
            objMarca.setDni(resultado.getString("dni"));
            objMarca.setNombre(resultado.getString("nombre"));
            listarPropietarios.add(objMarca);
        }
    }
    
    public void llenarComboPropietario(JComboBox combo) throws Exception
    {
        this.cargarDatos();
        combo.removeAllItems();
        for(int i = 0; i<listarPropietarios.size(); i++)
        {
            Propietario m = listarPropietarios.get(i);
            combo.addItem(m.getNombre());
        }
    }
    
}

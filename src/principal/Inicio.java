package principal;

import javax.swing.UIManager;
import presentacion.FrmMenuPrincipal;
//import presentacion.FrmIniciarSesion;

public class Inicio {

    public static void main(String[] args) {

        try {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        } catch (Exception e) {
            System.out.println("Ocurrió un error al cargar el tema");
        }

        //Instancia el formulario
        // FrmIniciarSesion objFrm = new FrmIniciarSesion();
        FrmMenuPrincipal objFrm = new FrmMenuPrincipal();
        //Muestra el formulario en pantalla
        objFrm.setVisible(true);

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author silviopd
 */
public class PropietarioAutos extends Conexion{
    
    private String dni,placa;
    private int id_propietario_auto;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getId_propietario_auto() {
        return id_propietario_auto;
    }

    public void setId_propietario_auto(int id_propietario_auto) {
        this.id_propietario_auto = id_propietario_auto;
    }
    
    public ResultSet listar() throws Exception {
        String sql = "SELECT \n" +
                    "  propietario_auto.id_propietario_auto, \n" +
                    "  propietario_auto.dni, \n" +
                    "  propietario_auto.placa, \n" +
                    "  propietario.nombre, \n" +
                    "  auto.marca, \n" +
                    "  auto.modelo\n" +
                    "FROM \n" +
                    "  public.auto, \n" +
                    "  public.propietario, \n" +
                    "  public.propietario_auto\n" +
                    "WHERE \n" +
                    "  auto.placa = propietario_auto.placa AND\n" +
                    "  propietario_auto.dni = propietario.dni;";

        PreparedStatement sp = this.abrirConexion().prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //PreparedStatement sp = this.abrirConexion().prepareStatement(sql);
        ResultSet resultado = sp.executeQuery();
        return resultado;
    }

    public boolean agregar() throws Exception {
        try {
            Connection transaccion = this.abrirConexion();
            transaccion.setAutoCommit(false);

            String sql = "select * from fn_propietarioauto_agregar(?,?)";
            PreparedStatement spInsertar = transaccion.prepareStatement(sql);
            spInsertar.setString(1, this.getDni());
            spInsertar.setString(2, this.getPlaca());
            ResultSet resultado = this.ejecutarSQL2(spInsertar);

            if (resultado.next()) {
                transaccion.commit();
            } else {
                transaccion.rollback();
            }

            transaccion.close();
            this.cerrarConexion(transaccion);

            return true;
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public boolean eliminar() throws Exception {
        Connection transaccion = abrirConexion();
        transaccion.setAutoCommit(false);

        String sql = "delete from propietario_auto where id_propietario_auto = ? and dni = ? and placa = ?";

        PreparedStatement sentencia = transaccion.prepareStatement(sql);
        sentencia.setInt(1, getId_propietario_auto());
        sentencia.setString(2, getDni());
        sentencia.setString(3, getPlaca());

        this.ejecutarSQL(sentencia, transaccion);
        transaccion.commit();
        transaccion.close();

        this.cerrarConexion(transaccion);

        return true;
    }
}
